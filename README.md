# cloud-engineer-tech-test

This is the Technical Task for prospective engineers applying for one of JLP's Cloud Engineering roles.

---

## Context

Your team has been working on a new feature that requires an `annotation` to be added to all our Kubernetes `Deployment` resources. Unfortunately the Developer who started the work is no longer available, and you have picked up the story to complete their work - including releasing it into Production.

The Platform to be deployed to is a Kubernetes cluster. The app does not have any other external dependencies.

---

## Brief

Your task is to build and deploy the application, and configure the Kubernetes **Mutating Webhook** included within this repository. Some work has been carried out to help get you started. The tasks remaining are:

1. Dockerise the existing Python application within this repo.
2. Assemble a CI/CD Pipeline within this repo to build and deploy the application. This should at a minimum include:
   - A stage to build the docker image and push it to a container registry.
   - A stage to deploy the application to a namespace `test`, for testing the application inside the cluster.
   - A stage to deploy the application to a namespace `prod`, the live copy of the application.

   You should add any other stages you feel are relevant as well.

   You can use any CI tool you are comfortable with to describe the stages - see [Things we are looking for](#things-we-are-looking-for) below for more detail on this.

   You can assume that the CI tooling takes care of logging into the container registry and Kubernetes context for you automatically - this part can be omitted.

3. The annotations should only be applied to `Namespaces` which have the label `type: tenant`.

**Note:** To help save you time, a script to configure the required TLS certificates is included in the `scripts/` directory.

---

## Getting Started

As a guide, you shouldn't expect to spend more than 2-3 hours on this task. If you find yourself needing longer, we suggest you capture your ideas for further improvements in the repository's README.

This repository contains the application to deploy as a starting point. Fork or clone this repo into your own GitLab namespace (you will need a GitLab account). You will share a link to this for us to review when you have finished.

We do not expect you to necessarily have ready access to a Kubernetes cluster in the cloud. For development purposes you may wish to use [minikube](https://minikube.sigs.k8s.io/docs/start/) or [kind](https://kind.sigs.k8s.io/) to test your work - we will use something similar to verify that your solution works.

You can use any CI tool you are comfortable with to produce the pipeline - there are many open source options to choose from. If it helps, we use Gitlab CI and Jenkins here at JLP - but other options are also perfectly fine.

### Local Development

If you wish to work with the Python code locally, you will need `python3.9`, `pip` and `pipenv`:

```bash
# setup
pip install pipenv
pipenv install --dev

# run
FLASK_APP=annotations_mutating_webhook pipenv run flask run    # the application should then be available on http://localhost:5000/
```

Some other helpful commands are available via `make`.

---

## Things We Are Looking For

We will primarily looking at the following:

1. Does your Dockerfile successfully build and run locally?
2. Does the application do what it is supposed to when deployed to a local Kubernetes cluster using the manifests you have created?
3. What steps you have included within your CI pipeline configuration. We are interested in the stages you have included and how you've organised the pipeline, not your knowledge of a particular CI tool or whether it is syntactically correct.

Please include any assumptions, instructions, or any other improvements or suggestions you feel are relevant in the README.

If time allows, you may want to also consider the following:

- What additional steps could be added to the pipeline to help assure the deployment is safe for Production?
- Do you have any suggested improvements to the Python application itself?
- Can you think of any ways that the application could be improved, or different approaches to solving this problem?
