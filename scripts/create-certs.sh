#!/usr/bin/env bash
set -eouE pipefail

SVC_NAME=${1}
NAMESPACE=${2}

echo "-> Generating new self-signed certificate ..."; echo

openssl genrsa -out ca.key 2048

openssl req -new -x509 -sha256 -days 365 -key ca.key \
  -subj "/C=GB/CN=${SVC_NAME}"\
  -out ca.crt

openssl req -newkey rsa:2048 -sha256 -nodes -keyout server.key \
  -subj "/C=GB/CN=${SVC_NAME}" \
  -out server.csr

openssl x509 -req \
  -extfile <(printf "subjectAltName=DNS:${SVC_NAME}.${NAMESPACE}.svc") \
  -sha256 \
  -days 365 \
  -in server.csr \
  -CA ca.crt -CAkey ca.key -CAcreateserial \
  -out server.crt

echo "-> Deleting existing secret if exists ..."; echo

kubectl delete secret "${SVC_NAME}"-certs --namespace="${NAMESPACE}" || true

echo "-> Creating kubernetes secrets ..."; echo

kubectl create secret tls "${SVC_NAME}"-certs \
  --cert=server.crt \
  --key=server.key \
  --namespace="${NAMESPACE}"

echo "-> Storing MutatingWebhookConfiguration caBundle in environment variable \$CA_BUNDLE"; echo

CA_BUNDLE=$(base64 < ca.crt)
export CA_BUNDLE

rm ca.crt ca.key ca.srl server.crt server.csr server.key
