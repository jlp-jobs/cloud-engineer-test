from annotations_mutating_webhook import app


def test_healthz():
    response = app.test_client().get('/healthz')
    data = response.data.decode("utf-8")
    assert data == "I'm alive!"
    assert response.status_code == 200
