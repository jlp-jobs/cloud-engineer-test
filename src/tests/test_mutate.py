from annotations_mutating_webhook.mutate import _has_required_annotations
from annotations_mutating_webhook import app
from annotations_mutating_webhook import POD_ANNOTATION
import json
import os
import pytest
import base64


@pytest.fixture
def admission_review():
    def _review_file(file):
        return json.load(open(os.path.join(os.path.dirname(__file__), f"fixtures/{file}.json")))

    return _review_file


def _get_decoded_patch(patch):
    return json.loads(base64.b64decode(patch).decode('utf8').replace("'", '"'))


def test_check_resource_has_pod_annotation_applied(admission_review):
    with app.app_context():
        app.config[POD_ANNOTATION] = {'cluster-autoscaler.kubernetes.io/safe-to-evict': 'true'}
        request_obj = admission_review('test-deployment-with-pod-annotation')
        print(request_obj)
        assert _has_required_annotations(request_obj['request']['object']['spec']['template']['metadata'],
                                         POD_ANNOTATION) is True


def test_mutated_patch_with_no_pod_annotation(admission_review):
    app.config[POD_ANNOTATION] = {'cluster-autoscaler.kubernetes.io/safe-to-evict': 'true'}
    request_obj = admission_review('test-deployment-no-annotations')
    response = app.test_client().post('/mutate/', json=request_obj)
    patch = _get_decoded_patch(response.json['response']['patch'])
    assert patch is not []
    assert patch == [
        {'op': 'add', 'path': '/spec/template/metadata/annotations',
         'value': {'cluster-autoscaler.kubernetes.io/safe-to-evict': 'true'}}
    ]
