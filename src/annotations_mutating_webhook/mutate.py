import base64
import jsonpatch
from flask import jsonify, request

from annotations_mutating_webhook import POD_ANNOTATION
from annotations_mutating_webhook import app


def process_resource():

    request_info = request.json
    app.logger.debug(f'Request Info {request_info}')

    request_obj = request_info['request']['object']

    uid = request_info["request"]["uid"]

    if not app.config[POD_ANNOTATION]:
        return _mutate(uid, [])

    patch = _get_patches(request_info['request'])

    app.logger.debug(f"Patch results {patch.apply(request_obj)}")

    return _mutate(uid, patch)


def _has_required_annotations(request_metadata, annotation):
    if required_annotations := app.config[annotation]:
        return all(
            item in request_metadata['annotations'].items()
            for item in required_annotations.items()
            ) if request_metadata.get('annotations') else False

    else:
        return True


def _create_patch(request_metadata, annotation_path):
    if _has_required_annotations(request_metadata, annotation_path):
        return
    if not app.config[annotation_path]:
        return
    annotations = app.config[annotation_path].copy()
    if request_annotation := request_metadata.get('annotations'):
        annotations.update(request_annotation)
    app.logger.debug(f'Annotations to patch {annotations}')
    return {'op': 'add', 'path': annotation_path, 'value': annotations}


def _get_patches(request_info):
    request_obj = request_info['object']
    patches = []

    if pod_patch := _create_patch(request_obj['spec']['template']['metadata'], POD_ANNOTATION):
        patches.append(pod_patch)
        app.logger.info(f"Deployment {request_obj['metadata']['name']} in {request_info['namespace']} namespace "
                        f"does not have the required annotation in the pod spec, mutating it !!")

    return jsonpatch.JsonPatch(patches)


def _mutate(uid, patch):

    admission_review = {
        "apiVersion": "admission.k8s.io/v1",
        "kind": "AdmissionReview",
        "response": {
            "uid": uid,
            "allowed": True,
            "patchType": "JSONPatch",
            "patch": base64.b64encode(str(patch).encode()).decode()
        }
    }

    app.logger.debug(jsonify(admission_review))

    return jsonify(admission_review)
