from os import getenv
import logging

# flake8: noqa
from flask import Flask

POD_ANNOTATION = '/spec/template/metadata/annotations'

app = Flask(__name__, instance_relative_config=True)

from annotations_mutating_webhook.mutate import process_resource


app.config['/spec/template/metadata/annotations'] = {'cluster-autoscaler.kubernetes.io/safe-to-evict': 'true'}


@app.route('/healthz')
def healthz():
    return "I'm alive!", 200


@app.route('/mutate/', methods=['POST'])
def mutate():
    return process_resource()


gunicorn_logger = logging.getLogger('root')

app.logger.propagate = False
app.logger.handlers = gunicorn_logger.handlers

if getenv("LOG_LEVEL") == "DEBUG":
    app.logger.setLevel(logging.DEBUG)
else:
    app.logger.setLevel(logging.INFO)

app.logger.info(f"Started {__name__}")
app.logger.debug("DEBUG logging enabled")


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
