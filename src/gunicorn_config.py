import hashlib
import logging
import os
import sys

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

access_log_format = '{ "remote_address": "%(h)s", "username": "%(u)s", "request_date": "%(t)s", "request": "%(r)s", ' \
                    '"request_method": "%(m)s", "path": "%(U)s", "query_string": "%(q)s", "protocol": "%(H)s", ' \
                    '"status": "%(s)s", "response_length": %(B)d, "referer": "%(f)s", "user_agent": "%(a)s", ' \
                    '"request_seconds": %(T)d, "request_microseconds": %(D)d, "request_decimal_seconds": "%(L)s", ' \
                    '"process_id": "%(p)s" }'  # noqa: E501


class ExitOnModifiedEventHandler(FileSystemEventHandler):
    def __init__(self, cert_path, logger=logging.getLogger()):
        self.logger = logger
        self.cert_path = cert_path
        self.cert_md5 = self.md5(cert_path)

    def md5(self, fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def on_modified(self, event):
        super(ExitOnModifiedEventHandler, self).on_modified(event)
        current_md5 = self.md5(self.cert_path)
        self.logger.info(
            f'CertWatcher: Previous md5: {self.cert_md5}. Current md5: {current_md5}. For cert {self.cert_path}')
        if self.cert_md5 != current_md5:
            self.logger.info(
                f'Certificate changed at {self.cert_path}. Restarting to pick up changes.')
            os._exit(0)


def _watch_cert(cert_path):
    event_handler = ExitOnModifiedEventHandler(cert_path)
    observer = Observer()
    observer.schedule(event_handler, os.path.dirname(cert_path))
    observer.start()


def _get_path_of_cert(arguments):
    return [arg.split("=")[1]
            for arg in arguments if arg.startswith("--certfile=")][0]


def on_starting(server):
    cert_path = _get_path_of_cert(sys.argv)
    _watch_cert(cert_path)
