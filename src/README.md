# annotation-mutating-webhook

This webhook was created to add the `cluster-autoscaler.kubernetes.io/safe-to-evict: "true"` to  all Kubernetes `Deployments` within `Namespaces` with the label `type: tenant`.
